clearvars;
close all;
format long

g = 9.81;

bag = rosbag('~/rosbag/still_imucam180221.bag');
imubag = select(bag, 'Topic', '/imuData0');
cambag = select(bag, 'Topic', '/car0PoseFromCam');
imubagdata = timeseries(imubag);
cambagdata = timeseries(cambag);

for i=1:length(imubagdata.Data)
    imudata(1,i) =  imubagdata.Data(i,4)*g;
    imudata(2,i) =  imubagdata.Data(i,5)*g;
    imudata(3,i) =  imubagdata.Data(i,6)*(pi/180);    
end
imu_cov = cov(imudata')
imu_cov = imu_cov.*eye(3)

for i=1:length(cambagdata.Data)
    camdata(1,i) =  cambagdata.Data(i,4)*g;
    camdata(2,i) =  cambagdata.Data(i,5)*g;
    camdata(3,i) =  cambagdata.Data(i,6)*(pi/180);    
end
cam_cov = cov(camdata')
cam_cov = cam_cov.*eye(3)


%% calculate means
imu_mean = mean(imudata(:, ~any(isnan(imudata), 1)), 2)

axis_color = [];
axis_color(1,:) = [1 0 0]; 
axis_color(2,:) = [0 1 0]; 
axis_color(3,:) = [0 0 1];
fade = 0.4;
frame_axis = {'x','y','z'};
frame_axis_angles = {'$\theta$','$\phi$','$\psi$'};

position = [100, 100, 1200, 400];

figure('Position', position);
axes
axis('square')  % [EDITED: or better 'square' !]
for i=1:3
  subplot(1,3,i)
  histogram(imudata(i,:),50,'Normalization','probability',...
            'facecolor',axis_color(i,:))
          title(frame_axis{i},'interpreter','latex')
end
%suptitle('Accelerometer measurements')



figure('Position', position);
axes
axis('square')  % [EDITED: or better 'square' !]
for i=1:3
  subplot(1,3,i)
  hold on
  plot(imubagdata.time,imudata(i,:),...
            'color',axis_color(i,:)*fade,...
            'displayname','Signal')
          
  p = polyfit( imubagdata.time,...
               imudata(i, :)',...
               1);
  x1 = polyval(p,imubagdata.time);
  plot(imubagdata.time,x1,...
       'color',axis_color(i,:),...
       'linewidth',2,...
       'linestyle','-.',...
       'displayname','Trend');
     leg_acc = legend('show');
          set(leg_acc,'interpreter','latex')
     xlabel('time (seconds)','interpreter','latex')
     ylabel('Force','interpreter','latex')
     title(frame_axis{i},'interpreter','latex')

end
%suptitle('Accelerometer measurements')
