# README #

### Matlab script for covariance analysis ###

Takes a rosbag, reads the data and calculates the covariance matrices

### How do I get set up? ###

Start matlab, change some names and paths if needed... run.

You'll need the Robotics toolbox.